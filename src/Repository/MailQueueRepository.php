<?php

namespace App\Repository;

use App\Entity\MailQueue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NoResultException;

/**
 * @method MailQueue|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailQueue|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailQueue[]    findAll()
 * @method MailQueue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailQueueRepository extends ServiceEntityRepository
{
     public function __construct(ManagerRegistry $registry)
     {
         parent::__construct($registry, MailQueue::class);
     }

     public function findMailStatusById(int $id): ?string
     {
         try {
             return $this->createQueryBuilder('mq')
                 ->select('mq.status')
                 ->where('mq.id = :id')
                 ->setParameter('id', $id)
                 ->getQuery()
                 ->getSingleScalarResult();
         } catch (NoResultException $e) {
             return null;
         }
     }
}
