<?php

namespace App\Handler;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;

class SlackMessageHandler
{
    /**
     * @var string
     */
    private $slackUrl;

    public function __construct(string $slackUrl)
    {
        $this->slackUrl = $slackUrl;
    }

    public function sendSlackMessage(string $username, string $text): void
    {
        $data = [
            'icon_emoji' => ':exclamation:',
            'username' => $username,
            'channel' => 'general',
            'mrkdwn' => true,
            'link_names' => true,
            'text' => $text
        ];

        $httpClient = HttpClient::create();
        $httpClient->request(
            Request::METHOD_POST,
            $this->slackUrl,
            [
                'headers' => ['Content-Type' => 'application/json',],
                'body' => json_encode($data)
            ]
        );
    }
}
