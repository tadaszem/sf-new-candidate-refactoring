<?php

namespace App\Handler;

use App\Entity\MailQueue;
use App\Enumerator\MailQueueStatus;
use App\Model\Request\AddEmailRequestModel;
use App\Repository\MailQueueRepository;
use App\Validator\MailQueueRequestValidator;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;

class MailQueueHandler
{
    /**
     * @var MailQueueRepository
     */
    private $mailQueueRepository;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MailQueueRequestValidator
     */
    private $requestValidator;

    public function __construct(
        MailQueueRepository $mailQueueRepository,
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        MailQueueRequestValidator $requestValidator
    ) {
        $this->mailQueueRepository = $mailQueueRepository;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
        $this->requestValidator = $requestValidator;
    }

    public function getById(int $id): ?MailQueue
    {
        return $this->mailQueueRepository->find($id);
    }

    public function getAll(): array
    {
        $mailQueues = $this->mailQueueRepository->findAll();

        return $this->serializer->toArray($mailQueues);
    }

    public function getMailStatus(int $id): ?string
    {
        return $this->mailQueueRepository->findMailStatusById($id);
    }

    public function updateMailStatus(int $id, string $status): ?MailQueue
    {
        $mail = $this->mailQueueRepository->find($id);

        if ($mail) {
            $mail->setStatus($status);
            $this->entityManager->flush();
        }

        return $mail;
    }

    /**
     * @return MailQueue[]
     */
    public function getAllNew(): array
    {
        return $this->mailQueueRepository->findBy(['status' => MailQueueStatus::NEW]);
    }

    public function addNewMessage(string $requestContent): int
    {
        $request = $this->serializer->deserialize($requestContent, AddEmailRequestModel::class, 'json');
        $this->requestValidator->validateRequest($request);

        $message = (new MailQueue())
            ->setTypes(implode('|', $request->getTypes()))
            ->setEmail($request->getEmail())
            ->setSubject($request->getSubject())
            ->setMessage($request->getMessage());
        $this->entityManager->persist($message);
        $this->entityManager->flush();

        return $message->getId();
    }
}
