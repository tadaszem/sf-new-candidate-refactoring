<?php

namespace App\Model\Request;

use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

class AddEmailRequestModel
{
    /**
     * @var array
     *
     * @Assert\NotBlank
     * @Assert\Choice(
     *     choices = { "email", "slack" },
     *     multiple = true,
     *     message = "Invalid type."
     * )
     * @JMS\Type("array<string>")
     */
    private $types;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @JMS\Type("string")
     */
    private $email;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @JMS\Type("string")
     */
    private $subject;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @JMS\Type("string")
     */
    private $message;

    public function getTypes(): array
    {
        return $this->types;
    }

    public function setTypes(array $types): AddEmailRequestModel
    {
        $this->types = $types;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): AddEmailRequestModel
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): AddEmailRequestModel
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): AddEmailRequestModel
    {
        $this->message = $message;

        return $this;
    }
}
