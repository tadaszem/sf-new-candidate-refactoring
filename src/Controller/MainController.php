<?php

namespace App\Controller;

use App\Enumerator\MailQueueStatus;
use App\Handler\MailQueueHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController
{
    /**
     * @var MailQueueHandler
     */
    private $mailQueueHandler;

    public function __construct(MailQueueHandler $mailQueueHandler)
    {
        $this->mailQueueHandler = $mailQueueHandler;
    }

    public function addEmail(Request $request)
    {
            $newMessageId = $this->mailQueueHandler->addNewMessage($request->getContent());

            return new JsonResponse(['success' => true, 'id' => $newMessageId]);
    }

    public function getEmail(int $id): Response
    {
        $mailQueue = $this->mailQueueHandler->getById($id);

        if ($mailQueue) {
            return $this->buildResponse($mailQueue->getMessage());
        }

        return new Response('Nothing found');
    }

    public function getAllEmails(): JsonResponse
    {
        $mailQueues = $this->mailQueueHandler->getAll();

        return $this->buildResponse($mailQueues);
    }

    public function getStatus(int $id): Response
    {
        if ($status = $this->mailQueueHandler->getMailStatus($id)) {
            return $this->buildStatusResponse($status);
        }

        return new Response('Message not found');
    }

    public function updateStatus(int $id): Response
    {
        $mail = $this->mailQueueHandler->updateMailStatus($id, MailQueueStatus::CANCELED);

        if ($mail) {
            return $this->buildStatusResponse($mail->getStatus());
        }

        return new Response('Failed to retrieve mail');
    }

    /**
     * @param array|string $body
     */
    private function buildResponse($body): JsonResponse
    {
        return new JsonResponse(['success' => true, 'body' => $body]);
    }

    private function buildStatusResponse(string $status): JsonResponse
    {
        return new JsonResponse(['success' => true, 'status' => $status]);
    }
}
