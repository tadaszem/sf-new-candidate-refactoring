<?php

namespace App\Validator;

use App\Model\Request\AddEmailRequestModel;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class MailQueueRequestValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validateRequest(AddEmailRequestModel $request)
    {
        $errors = $this->validator->validate($request);
        if (count($errors) > 0) {
            throw new BadRequestHttpException(
                sprintf('Invalid field - %s: %s', $errors[0]->getPropertyPath(), $errors[0]->getMessage())
            );
        }
    }
}
