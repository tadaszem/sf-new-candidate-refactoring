<?php

declare(strict_types=1);

namespace App\Command;

use App\Enumerator\MailQueueStatus;
use App\Enumerator\MailQueueType;
use App\Handler\MailQueueHandler;
use App\Handler\SlackMessageHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMessages extends Command
{
    public static $defaultName = 'mail:send';

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var MailQueueHandler
     */
    private $mailQueueHandler;

    /**
     * @var SlackMessageHandler
     */
    private $slackMessageHandler;

    public function __construct(
        EntityManagerInterface $entityManager,
        MailQueueHandler $mailQueueHandler,
        SlackMessageHandler $slackMessageHandler
    ) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->mailQueueHandler = $mailQueueHandler;
        $this->slackMessageHandler = $slackMessageHandler;
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $messages = $this->mailQueueHandler->getAllNew();
        foreach ($messages as $message) {
            if (strpos($message->getTypes(), MailQueueType::MAIL) !== false) {
                mail($message->getEmail(), $message->getSubject(), $message->getMessage());
            }

            if (strpos($message->getTypes(), MailQueueType::SLACK) !== false) {
                $this->slackMessageHandler->sendSlackMessage($message->getSubject(), $message->getMessage());
            }

            $message->setStatus(MailQueueStatus::SENT);
        }
        $this->entityManager->flush();

        $output->writeln('Messages sent successfully!');
    }
}
