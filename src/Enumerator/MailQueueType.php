<?php

namespace App\Enumerator;

class MailQueueType
{
    public const SLACK = 'slack';
    public const MAIL = 'mail';
}
