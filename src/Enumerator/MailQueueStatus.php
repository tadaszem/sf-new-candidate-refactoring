<?php

namespace App\Enumerator;

class MailQueueStatus
{
    public const NEW = 'new';
    public const CANCELED = 'canceled';
    public const SENT = 'sent';
}
